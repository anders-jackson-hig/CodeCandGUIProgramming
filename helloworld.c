#include <stdio.h>
#include <stdlib.h>

/*
 * Test program that should be compiled with 'make all' in command shell
 * If the Makefile is correct, you will have a program 'helloworld'
 * Execute the program in command shell with './helloworld'
 */

int main (void)
{
  /* A print statement */
  printf("Hello world!\n");
  exit(0);
}
