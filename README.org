# -*- org-mode -*-
#+TITLE: Code from 'An Introduction to C & GUI Programming'
#+AUTHOR: Anders Jackson
#+LANGUAGE: english
#+OPTIONS: toc:nil num:nil

* An Introduction to  C & GUI Programming

  This is my set on the code examples in the book "/An Introduction to
  C & GUI Programming/" by /Simon Long/, Raspberry Pi Press, ISBN:
  978-1-912047-65-9.  The book is sold by Raspberry Pi Foundation and
  can be found at [[https://store.rpipress.cc/products/an-introduction-to-c-gui-programming][Raspberry Pi Press]] store.

  It is also free to download from [[https://magpi.raspberrypi.org/books/c-gui-programming][MagPi website]].  But if you can, buy
  it and support /Raspberry Pi Foundation/ and /Simon Long/.

* The C Code

  Most of the C code from the book are written and adjusted to
  compile.  That gives there are some differences between my code and
  the one in the book.

  Each chapter in the book has a corresponding directory.  So it
  should be quite easy to find which directory a code are located.

  I have provided them with a =Makefile= in each directory, so each
  program can be compiled with the =make(1)= command in the usual way
  for C programs. I have put some work into write the =Makefile=, so
  I would be pleased if you use them in your projects.

** Programming Environment

  To be able to compile, the right development libraries need to be
  installed.  You should try theses commands on your Raspberry Pi
  running Raspbian, or any Debian base Linux distribution, like
  Ubuntu.

  About how to install these libraries in any other distributions?
  Sorry, doesn't have a clue about how they work.  You should be able
  to figure it out anyways.  I don't know.

  #+CAPTION: Installation of programming libraries
  #+BEGIN_SRC sh
    sudo apt update
    sudo apt upgrade
    sudo apt install build-essential git # compilers, make and git
    sudo apt install libgtk2.0-dev       # gtk 2.0 development
  #+END_SRC

  After this C compiler =gcc(1)=, dependency and build tool =make(1)=,
  version handling tool =git(1)= and =gtk2.0= development libraries
  installed.

  After this, one can test if it work by compiling =helloworld.c= from
  the same directory where it is, by typing this in a shell.

  #+CAPTION: Testing compiler and make installation
  #+BEGIN_SRC sh
    make helloworld
    ./helloworld
  #+END_SRC

  By the way, I uses Emacs when coding.  Great editor, you should try.
  You can try [[https://gitlab.com/anders-jackson-hig/emacs-init][my Emacs settings at GitLab]]. if you want some more then
  only vanilla settings for Emacs.

** Compiling the Code

  So to compile all programs in any chapter, like chapter 15, just
  follow this example:

  #+CAPTION: Example of compiling some example codes
  #+BEGIN_SRC sh
    cd Chapter-15
    make clean  # remove all old compilations
    ls          # have a look at what is there
    make all    # compile all programs
    ls          # programs should be there
    ./button_two &
    ls
  #+END_SRC

  The last line ends with a =&= character which tells the commando
  shell not to wait for the program to finish. Try with, and without,
  the =&= character and type in the command =ls= after, before exiting
  =./button_two=. One will execute the =ls=-command immediate, the
  other will wait until the graphical program exits.

* Rest

  There might be some typing errors, and some other issues.  But I
  tried to follow the book as close as I could, but still be able to
  compile the examples as a program.

  I don't have copyright for the code, but for the =Makefile=.  And
  you are welcome to use that as you wish. Everything in it can be
  found in manual and on Internet with search engines.

  We should also show our gratitude to Simon Long and Raspberry Pi
  Foundation for put the time and effort to make the book and write
  the original code.

  Thank you Simon

# Local Variables: 
# ispell-local-dictionary: english
# End
