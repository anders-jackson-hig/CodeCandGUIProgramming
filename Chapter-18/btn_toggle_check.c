#include <gtk/gtk.h>

void end_program (GtkWidget *wid, gpointer ptr)
{
  gtk_main_quit ();
}

void check_toggle (GtkWidget *wid, gpointer ptr)
{
  printf("The state of the button is %d\n",
         gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (wid)));
}

int main (int argc, char *argv[])
{
  gtk_init (&argc, &argv);

  GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (win, "delete_event", G_CALLBACK (end_program), NULL);

  GtkWidget *btn = gtk_button_new_with_label ("Close window");
  g_signal_connect (btn, "clicked", G_CALLBACK (end_program), NULL);

  GtkWidget *lbl = gtk_label_new ("My label");

  GtkWidget *chk = gtk_check_button_new_with_label ("My check");
  g_signal_connect (chk, "toggled", G_CALLBACK (check_toggle), NULL);

  GtkObject *adj = gtk_adjustment_new (0, -10, 10, 1, 0, 0);
  GtkWidget *txt = gtk_spin_button_new (GTK_ADJUSTMENT (adj), 0, 0);

  GtkWidget *tab = gtk_table_new (2, 2, TRUE);
  gtk_table_attach_defaults (GTK_TABLE (tab), lbl, 0, 1, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE (tab), chk, 1, 2, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE (tab), btn, 0, 1, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE (tab), txt, 1, 2, 1, 2);
  gtk_container_add (GTK_CONTAINER (win), tab);

  gtk_widget_show_all (win);
  gtk_main ();
}
